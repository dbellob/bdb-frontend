import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Person } from './person';


@Injectable({
  providedIn: 'root'
})
export class PersonService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }


  add(person: Person): Observable<Person> {
    return this.http.post<Person>('http://localhost:8085/api/v1/person/save', person, this.httpOptions).pipe(
      catchError(this.handleError<Person>('addPerson'))
    );
  }

  get(): Observable<Person[]> {
    return this.http.get<Person[]>('http://localhost:8085/api/v1/person')
      .pipe(
        catchError(this.handleError<Person[]>('getPerson', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }

  

  
}
