import { Component, OnInit } from '@angular/core';
import { PersonService } from './person.service';
import { Person } from './person';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'person';
  regForm;
  persons: Person[];

  constructor(private personService: PersonService,  private formBuilder: FormBuilder) { 

    this.regForm = this.formBuilder.group({
      fullname: '',
      birth: '',
    });

  }

  ngOnInit() {
    this.get();
  }

  get(): void {
    this.personService.get()
    .subscribe(personsQ => this.persons = personsQ);
  }

  add(
    fullname: string,
    birth: Date,
    idfather:  number,
    idmather: number,
    idchild: number): void {
      fullname = fullname.trim();
    if (!fullname) { return; }
    this.personService.add({ fullname,  birth, idfather, idmather, idchild} as Person)
      .subscribe(person => {
        window.location.reload();
      });
  }

  onSubmit(person) {
    this.add(person.fullname, person.birth, 0,0,0)
  }
}
